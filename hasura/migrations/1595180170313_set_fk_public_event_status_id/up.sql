alter table "public"."event"
           add constraint "event_status_id_fkey"
           foreign key ("status_id")
           references "public"."status"
           ("status_id") on update restrict on delete restrict;
