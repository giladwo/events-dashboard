Setup steps:
1. Install hasura-cli:
```bash
$ npm i -g hasura-cli
```
2. Install podman + podman-compose or docker + docker-compose, i.e.:
```bash
# dnf install podman
# pip install podman-compose
```
3. Clone this project
4. Start the containers:
```bash
$ cd <cloned project root>
$ podman-compose up -d
```
5. Apply migrations and metadata
```bash
$ cd hasura
$ hasura migrate apply
$ hasura metadata apply
```


To start hasura console:
```bash
$ cd hasura
$ hasura console
```
