#!/usr/bin/env bash

QUERY='mutation InsertEvent($annotation: String!, $tag_id: Int!, $origin: name!, $status_id: Int!, $title: String!) {
  insert_event(objects: {annotation: $annotation, event_tags: {data: {tag_id: $tag_id}}, origin: $origin, status_id: $status_id, title: $title}) {
    returning {
      event_id
      title
      status {
        status_name
      }
      origin
      report_time
      expiration
      annotation
      event_tags {
        tag {
          tag_name
        }
      }
    }
  }
}'

if [ "$#" -lt 5 ]; then
  echo "USAGE: post_event.sh <title> <annotation> <origin> <status_id> <tag_id> [uri]"
  exit 1
fi

URL="${6:-http://localhost:8080/v1/graphql}"

gq "$URL" -q "$QUERY" -v "title=$1" -v "annotation=$2" -v "origin=$3" -v "status_id=$4" -v "tag_id=$5"
