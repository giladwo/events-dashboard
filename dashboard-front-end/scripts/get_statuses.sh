#!/usr/bin/env bash

QUERY='query GetStatuses {
  status {
    status_id
    status_name
  }
}'

URL="${1:-http://localhost:8080/v1/graphql}"

gq "$URL" -q "$QUERY"
