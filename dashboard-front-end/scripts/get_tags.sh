#!/usr/bin/env bash

QUERY='query GetTags {
  tag {
    tag_id
    tag_name
  }
}'

URL="${1:-http://localhost:8080/v1/graphql}"

gq "$URL" -q "$QUERY"
