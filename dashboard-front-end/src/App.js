/** @jsx jsx */
import logo from './logo.svg';
import { css, jsx, keyframes } from '@emotion/core';
import { useSubscription, gql } from '@apollo/client';

const SUBSCRIBE_EVENTS = gql`
  subscription SubscribeEvents {
    event {
      event_id
      annotation
      expiration
      origin
      report_time
      title
      status {
        status_id
        status_name
      }
    }
  }
`;

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Events = () => {
  const { data } = useSubscription(SUBSCRIBE_EVENTS);
  return (
    <pre>
      {JSON.stringify(data, null, 2)}
    </pre>
  );
};

const App = () => {
  return (
    <div>
      <header css={css`
          background-color: #282c34;
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          font-size: calc(10px + 2vmin);
          color: white;
      `}>
        <img src={logo} css={css`
            height: 40vmin;
            pointer-events: none;
            @media (prefers-reduced-motion: no-preference) {              
              animation: ${spin} infinite 20s linear;              
            }
        `} alt="logo" />
        <Events />
        <a
          css={css`
            color: #61dafb;
          `}
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
