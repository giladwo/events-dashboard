import { gql } from "@apollo/client";

const typeDefs = gql`
  schema {
    query: query_root
    mutation: mutation_root
    subscription: subscription_root
  }

  # columns and relationships of "event"
  type event {
    annotation: String!
    event_id: Int!

    # An array relationship
    event_tags(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): [event_tag!]!

    # An aggregated array relationship
    event_tags_aggregate(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): event_tag_aggregate!
    expiration: interval!
    origin: name!
    report_time: timestamptz!

    # An object relationship
    status: status!
    status_id: Int!
    title: String!
  }

  # aggregated selection of "event"
  type event_aggregate {
    aggregate: event_aggregate_fields
    nodes: [event!]!
  }

  # aggregate fields of "event"
  type event_aggregate_fields {
    avg: event_avg_fields
    count(columns: [event_select_column!], distinct: Boolean): Int
    max: event_max_fields
    min: event_min_fields
    stddev: event_stddev_fields
    stddev_pop: event_stddev_pop_fields
    stddev_samp: event_stddev_samp_fields
    sum: event_sum_fields
    var_pop: event_var_pop_fields
    var_samp: event_var_samp_fields
    variance: event_variance_fields
  }

  # order by aggregate values of table "event"
  input event_aggregate_order_by {
    avg: event_avg_order_by
    count: order_by
    max: event_max_order_by
    min: event_min_order_by
    stddev: event_stddev_order_by
    stddev_pop: event_stddev_pop_order_by
    stddev_samp: event_stddev_samp_order_by
    sum: event_sum_order_by
    var_pop: event_var_pop_order_by
    var_samp: event_var_samp_order_by
    variance: event_variance_order_by
  }

  # input type for inserting array relation for remote table "event"
  input event_arr_rel_insert_input {
    data: [event_insert_input!]!
    on_conflict: event_on_conflict
  }

  # aggregate avg on columns
  type event_avg_fields {
    event_id: Float
    status_id: Float
  }

  # order by avg() on columns of table "event"
  input event_avg_order_by {
    event_id: order_by
    status_id: order_by
  }

  # Boolean expression to filter rows from the table "event". All fields are combined with a logical 'AND'.
  input event_bool_exp {
    _and: [event_bool_exp]
    _not: event_bool_exp
    _or: [event_bool_exp]
    annotation: String_comparison_exp
    event_id: Int_comparison_exp
    event_tags: event_tag_bool_exp
    expiration: interval_comparison_exp
    origin: name_comparison_exp
    report_time: timestamptz_comparison_exp
    status: status_bool_exp
    status_id: Int_comparison_exp
    title: String_comparison_exp
  }

  # unique or primary key constraints on table "event"
  enum event_constraint {
    # unique or primary key constraint
    event_pkey
  }

  # input type for incrementing integer column in table "event"
  input event_inc_input {
    event_id: Int
    status_id: Int
  }

  # input type for inserting data into table "event"
  input event_insert_input {
    annotation: String
    event_id: Int
    event_tags: event_tag_arr_rel_insert_input
    expiration: interval
    origin: name
    report_time: timestamptz
    status: status_obj_rel_insert_input
    status_id: Int
    title: String
  }

  # aggregate max on columns
  type event_max_fields {
    annotation: String
    event_id: Int
    report_time: timestamptz
    status_id: Int
    title: String
  }

  # order by max() on columns of table "event"
  input event_max_order_by {
    annotation: order_by
    event_id: order_by
    report_time: order_by
    status_id: order_by
    title: order_by
  }

  # aggregate min on columns
  type event_min_fields {
    annotation: String
    event_id: Int
    report_time: timestamptz
    status_id: Int
    title: String
  }

  # order by min() on columns of table "event"
  input event_min_order_by {
    annotation: order_by
    event_id: order_by
    report_time: order_by
    status_id: order_by
    title: order_by
  }

  # response of any mutation on the table "event"
  type event_mutation_response {
    # number of affected rows by the mutation
    affected_rows: Int!

    # data of the affected rows by the mutation
    returning: [event!]!
  }

  # input type for inserting object relation for remote table "event"
  input event_obj_rel_insert_input {
    data: event_insert_input!
    on_conflict: event_on_conflict
  }

  # on conflict condition type for table "event"
  input event_on_conflict {
    constraint: event_constraint!
    update_columns: [event_update_column!]!
    where: event_bool_exp
  }

  # ordering options when selecting data from "event"
  input event_order_by {
    annotation: order_by
    event_id: order_by
    event_tags_aggregate: event_tag_aggregate_order_by
    expiration: order_by
    origin: order_by
    report_time: order_by
    status: status_order_by
    status_id: order_by
    title: order_by
  }

  # primary key columns input for table: "event"
  input event_pk_columns_input {
    event_id: Int!
  }

  # select columns of table "event"
  enum event_select_column {
    # column name
    annotation

    # column name
    event_id

    # column name
    expiration

    # column name
    origin

    # column name
    report_time

    # column name
    status_id

    # column name
    title
  }

  # input type for updating data in table "event"
  input event_set_input {
    annotation: String
    event_id: Int
    expiration: interval
    origin: name
    report_time: timestamptz
    status_id: Int
    title: String
  }

  # aggregate stddev on columns
  type event_stddev_fields {
    event_id: Float
    status_id: Float
  }

  # order by stddev() on columns of table "event"
  input event_stddev_order_by {
    event_id: order_by
    status_id: order_by
  }

  # aggregate stddev_pop on columns
  type event_stddev_pop_fields {
    event_id: Float
    status_id: Float
  }

  # order by stddev_pop() on columns of table "event"
  input event_stddev_pop_order_by {
    event_id: order_by
    status_id: order_by
  }

  # aggregate stddev_samp on columns
  type event_stddev_samp_fields {
    event_id: Float
    status_id: Float
  }

  # order by stddev_samp() on columns of table "event"
  input event_stddev_samp_order_by {
    event_id: order_by
    status_id: order_by
  }

  # aggregate sum on columns
  type event_sum_fields {
    event_id: Int
    status_id: Int
  }

  # order by sum() on columns of table "event"
  input event_sum_order_by {
    event_id: order_by
    status_id: order_by
  }

  # columns and relationships of "event_tag"
  type event_tag {
    # An object relationship
    event: event!
    event_id: Int!
    event_tag_id: Int!

    # An object relationship
    tag: tag!
    tag_id: Int!
  }

  # aggregated selection of "event_tag"
  type event_tag_aggregate {
    aggregate: event_tag_aggregate_fields
    nodes: [event_tag!]!
  }

  # aggregate fields of "event_tag"
  type event_tag_aggregate_fields {
    avg: event_tag_avg_fields
    count(columns: [event_tag_select_column!], distinct: Boolean): Int
    max: event_tag_max_fields
    min: event_tag_min_fields
    stddev: event_tag_stddev_fields
    stddev_pop: event_tag_stddev_pop_fields
    stddev_samp: event_tag_stddev_samp_fields
    sum: event_tag_sum_fields
    var_pop: event_tag_var_pop_fields
    var_samp: event_tag_var_samp_fields
    variance: event_tag_variance_fields
  }

  # order by aggregate values of table "event_tag"
  input event_tag_aggregate_order_by {
    avg: event_tag_avg_order_by
    count: order_by
    max: event_tag_max_order_by
    min: event_tag_min_order_by
    stddev: event_tag_stddev_order_by
    stddev_pop: event_tag_stddev_pop_order_by
    stddev_samp: event_tag_stddev_samp_order_by
    sum: event_tag_sum_order_by
    var_pop: event_tag_var_pop_order_by
    var_samp: event_tag_var_samp_order_by
    variance: event_tag_variance_order_by
  }

  # input type for inserting array relation for remote table "event_tag"
  input event_tag_arr_rel_insert_input {
    data: [event_tag_insert_input!]!
    on_conflict: event_tag_on_conflict
  }

  # aggregate avg on columns
  type event_tag_avg_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by avg() on columns of table "event_tag"
  input event_tag_avg_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # Boolean expression to filter rows from the table "event_tag". All fields are combined with a logical 'AND'.
  input event_tag_bool_exp {
    _and: [event_tag_bool_exp]
    _not: event_tag_bool_exp
    _or: [event_tag_bool_exp]
    event: event_bool_exp
    event_id: Int_comparison_exp
    event_tag_id: Int_comparison_exp
    tag: tag_bool_exp
    tag_id: Int_comparison_exp
  }

  # unique or primary key constraints on table "event_tag"
  enum event_tag_constraint {
    # unique or primary key constraint
    event_tag_event_id_tag_id_key

    # unique or primary key constraint
    event_tag_pkey
  }

  # input type for incrementing integer column in table "event_tag"
  input event_tag_inc_input {
    event_id: Int
    event_tag_id: Int
    tag_id: Int
  }

  # input type for inserting data into table "event_tag"
  input event_tag_insert_input {
    event: event_obj_rel_insert_input
    event_id: Int
    event_tag_id: Int
    tag: tag_obj_rel_insert_input
    tag_id: Int
  }

  # aggregate max on columns
  type event_tag_max_fields {
    event_id: Int
    event_tag_id: Int
    tag_id: Int
  }

  # order by max() on columns of table "event_tag"
  input event_tag_max_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate min on columns
  type event_tag_min_fields {
    event_id: Int
    event_tag_id: Int
    tag_id: Int
  }

  # order by min() on columns of table "event_tag"
  input event_tag_min_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # response of any mutation on the table "event_tag"
  type event_tag_mutation_response {
    # number of affected rows by the mutation
    affected_rows: Int!

    # data of the affected rows by the mutation
    returning: [event_tag!]!
  }

  # input type for inserting object relation for remote table "event_tag"
  input event_tag_obj_rel_insert_input {
    data: event_tag_insert_input!
    on_conflict: event_tag_on_conflict
  }

  # on conflict condition type for table "event_tag"
  input event_tag_on_conflict {
    constraint: event_tag_constraint!
    update_columns: [event_tag_update_column!]!
    where: event_tag_bool_exp
  }

  # ordering options when selecting data from "event_tag"
  input event_tag_order_by {
    event: event_order_by
    event_id: order_by
    event_tag_id: order_by
    tag: tag_order_by
    tag_id: order_by
  }

  # primary key columns input for table: "event_tag"
  input event_tag_pk_columns_input {
    event_tag_id: Int!
  }

  # select columns of table "event_tag"
  enum event_tag_select_column {
    # column name
    event_id

    # column name
    event_tag_id

    # column name
    tag_id
  }

  # input type for updating data in table "event_tag"
  input event_tag_set_input {
    event_id: Int
    event_tag_id: Int
    tag_id: Int
  }

  # aggregate stddev on columns
  type event_tag_stddev_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by stddev() on columns of table "event_tag"
  input event_tag_stddev_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate stddev_pop on columns
  type event_tag_stddev_pop_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by stddev_pop() on columns of table "event_tag"
  input event_tag_stddev_pop_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate stddev_samp on columns
  type event_tag_stddev_samp_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by stddev_samp() on columns of table "event_tag"
  input event_tag_stddev_samp_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate sum on columns
  type event_tag_sum_fields {
    event_id: Int
    event_tag_id: Int
    tag_id: Int
  }

  # order by sum() on columns of table "event_tag"
  input event_tag_sum_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # update columns of table "event_tag"
  enum event_tag_update_column {
    # column name
    event_id

    # column name
    event_tag_id

    # column name
    tag_id
  }

  # aggregate var_pop on columns
  type event_tag_var_pop_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by var_pop() on columns of table "event_tag"
  input event_tag_var_pop_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate var_samp on columns
  type event_tag_var_samp_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by var_samp() on columns of table "event_tag"
  input event_tag_var_samp_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # aggregate variance on columns
  type event_tag_variance_fields {
    event_id: Float
    event_tag_id: Float
    tag_id: Float
  }

  # order by variance() on columns of table "event_tag"
  input event_tag_variance_order_by {
    event_id: order_by
    event_tag_id: order_by
    tag_id: order_by
  }

  # update columns of table "event"
  enum event_update_column {
    # column name
    annotation

    # column name
    event_id

    # column name
    expiration

    # column name
    origin

    # column name
    report_time

    # column name
    status_id

    # column name
    title
  }

  # aggregate var_pop on columns
  type event_var_pop_fields {
    event_id: Float
    status_id: Float
  }

  # order by var_pop() on columns of table "event"
  input event_var_pop_order_by {
    event_id: order_by
    status_id: order_by
  }

  # aggregate var_samp on columns
  type event_var_samp_fields {
    event_id: Float
    status_id: Float
  }

  # order by var_samp() on columns of table "event"
  input event_var_samp_order_by {
    event_id: order_by
    status_id: order_by
  }

  # aggregate variance on columns
  type event_variance_fields {
    event_id: Float
    status_id: Float
  }

  # order by variance() on columns of table "event"
  input event_variance_order_by {
    event_id: order_by
    status_id: order_by
  }

  # expression to compare columns of type Int. All fields are combined with logical 'AND'.
  input Int_comparison_exp {
    _eq: Int
    _gt: Int
    _gte: Int
    _in: [Int!]
    _is_null: Boolean
    _lt: Int
    _lte: Int
    _neq: Int
    _nin: [Int!]
  }

  scalar interval

  # expression to compare columns of type interval. All fields are combined with logical 'AND'.
  input interval_comparison_exp {
    _eq: interval
    _gt: interval
    _gte: interval
    _in: [interval!]
    _is_null: Boolean
    _lt: interval
    _lte: interval
    _neq: interval
    _nin: [interval!]
  }

  # mutation root
  type mutation_root {
    # delete data from the table: "event"
    delete_event(
      # filter the rows which have to be deleted
      where: event_bool_exp!
    ): event_mutation_response

    # delete single row from the table: "event"
    delete_event_by_pk(event_id: Int!): event

    # delete data from the table: "event_tag"
    delete_event_tag(
      # filter the rows which have to be deleted
      where: event_tag_bool_exp!
    ): event_tag_mutation_response

    # delete single row from the table: "event_tag"
    delete_event_tag_by_pk(event_tag_id: Int!): event_tag

    # delete data from the table: "status"
    delete_status(
      # filter the rows which have to be deleted
      where: status_bool_exp!
    ): status_mutation_response

    # delete single row from the table: "status"
    delete_status_by_pk(status_id: Int!): status

    # delete data from the table: "tag"
    delete_tag(
      # filter the rows which have to be deleted
      where: tag_bool_exp!
    ): tag_mutation_response

    # delete single row from the table: "tag"
    delete_tag_by_pk(tag_id: Int!): tag

    # insert data into the table: "event"
    insert_event(
      # the rows to be inserted
      objects: [event_insert_input!]!

      # on conflict condition
      on_conflict: event_on_conflict
    ): event_mutation_response

    # insert a single row into the table: "event"
    insert_event_one(
      # the row to be inserted
      object: event_insert_input!

      # on conflict condition
      on_conflict: event_on_conflict
    ): event

    # insert data into the table: "event_tag"
    insert_event_tag(
      # the rows to be inserted
      objects: [event_tag_insert_input!]!

      # on conflict condition
      on_conflict: event_tag_on_conflict
    ): event_tag_mutation_response

    # insert a single row into the table: "event_tag"
    insert_event_tag_one(
      # the row to be inserted
      object: event_tag_insert_input!

      # on conflict condition
      on_conflict: event_tag_on_conflict
    ): event_tag

    # insert data into the table: "status"
    insert_status(
      # the rows to be inserted
      objects: [status_insert_input!]!

      # on conflict condition
      on_conflict: status_on_conflict
    ): status_mutation_response

    # insert a single row into the table: "status"
    insert_status_one(
      # the row to be inserted
      object: status_insert_input!

      # on conflict condition
      on_conflict: status_on_conflict
    ): status

    # insert data into the table: "tag"
    insert_tag(
      # the rows to be inserted
      objects: [tag_insert_input!]!

      # on conflict condition
      on_conflict: tag_on_conflict
    ): tag_mutation_response

    # insert a single row into the table: "tag"
    insert_tag_one(
      # the row to be inserted
      object: tag_insert_input!

      # on conflict condition
      on_conflict: tag_on_conflict
    ): tag

    # update data of the table: "event"
    update_event(
      # increments the integer columns with given value of the filtered values
      _inc: event_inc_input

      # sets the columns of the filtered rows to the given values
      _set: event_set_input

      # filter the rows which have to be updated
      where: event_bool_exp!
    ): event_mutation_response

    # update single row of the table: "event"
    update_event_by_pk(
      # increments the integer columns with given value of the filtered values
      _inc: event_inc_input

      # sets the columns of the filtered rows to the given values
      _set: event_set_input
      pk_columns: event_pk_columns_input!
    ): event

    # update data of the table: "event_tag"
    update_event_tag(
      # increments the integer columns with given value of the filtered values
      _inc: event_tag_inc_input

      # sets the columns of the filtered rows to the given values
      _set: event_tag_set_input

      # filter the rows which have to be updated
      where: event_tag_bool_exp!
    ): event_tag_mutation_response

    # update single row of the table: "event_tag"
    update_event_tag_by_pk(
      # increments the integer columns with given value of the filtered values
      _inc: event_tag_inc_input

      # sets the columns of the filtered rows to the given values
      _set: event_tag_set_input
      pk_columns: event_tag_pk_columns_input!
    ): event_tag

    # update data of the table: "status"
    update_status(
      # increments the integer columns with given value of the filtered values
      _inc: status_inc_input

      # sets the columns of the filtered rows to the given values
      _set: status_set_input

      # filter the rows which have to be updated
      where: status_bool_exp!
    ): status_mutation_response

    # update single row of the table: "status"
    update_status_by_pk(
      # increments the integer columns with given value of the filtered values
      _inc: status_inc_input

      # sets the columns of the filtered rows to the given values
      _set: status_set_input
      pk_columns: status_pk_columns_input!
    ): status

    # update data of the table: "tag"
    update_tag(
      # increments the integer columns with given value of the filtered values
      _inc: tag_inc_input

      # sets the columns of the filtered rows to the given values
      _set: tag_set_input

      # filter the rows which have to be updated
      where: tag_bool_exp!
    ): tag_mutation_response

    # update single row of the table: "tag"
    update_tag_by_pk(
      # increments the integer columns with given value of the filtered values
      _inc: tag_inc_input

      # sets the columns of the filtered rows to the given values
      _set: tag_set_input
      pk_columns: tag_pk_columns_input!
    ): tag
  }

  scalar name

  # expression to compare columns of type name. All fields are combined with logical 'AND'.
  input name_comparison_exp {
    _eq: name
    _gt: name
    _gte: name
    _in: [name!]
    _is_null: Boolean
    _lt: name
    _lte: name
    _neq: name
    _nin: [name!]
  }

  # column ordering options
  enum order_by {
    # in the ascending order, nulls last
    asc

    # in the ascending order, nulls first
    asc_nulls_first

    # in the ascending order, nulls last
    asc_nulls_last

    # in the descending order, nulls first
    desc

    # in the descending order, nulls first
    desc_nulls_first

    # in the descending order, nulls last
    desc_nulls_last
  }

  # query root
  type query_root {
    # fetch data from the table: "event"
    event(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): [event!]!

    # fetch aggregated fields from the table: "event"
    event_aggregate(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): event_aggregate!

    # fetch data from the table: "event" using primary key columns
    event_by_pk(event_id: Int!): event

    # fetch data from the table: "event_tag"
    event_tag(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): [event_tag!]!

    # fetch aggregated fields from the table: "event_tag"
    event_tag_aggregate(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): event_tag_aggregate!

    # fetch data from the table: "event_tag" using primary key columns
    event_tag_by_pk(event_tag_id: Int!): event_tag

    # fetch data from the table: "status"
    status(
      # distinct select on columns
      distinct_on: [status_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [status_order_by!]

      # filter the rows returned
      where: status_bool_exp
    ): [status!]!

    # fetch aggregated fields from the table: "status"
    status_aggregate(
      # distinct select on columns
      distinct_on: [status_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [status_order_by!]

      # filter the rows returned
      where: status_bool_exp
    ): status_aggregate!

    # fetch data from the table: "status" using primary key columns
    status_by_pk(status_id: Int!): status

    # fetch data from the table: "tag"
    tag(
      # distinct select on columns
      distinct_on: [tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [tag_order_by!]

      # filter the rows returned
      where: tag_bool_exp
    ): [tag!]!

    # fetch aggregated fields from the table: "tag"
    tag_aggregate(
      # distinct select on columns
      distinct_on: [tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [tag_order_by!]

      # filter the rows returned
      where: tag_bool_exp
    ): tag_aggregate!

    # fetch data from the table: "tag" using primary key columns
    tag_by_pk(tag_id: Int!): tag
  }

  # columns and relationships of "status"
  type status {
    # An array relationship
    events(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): [event!]!

    # An aggregated array relationship
    events_aggregate(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): event_aggregate!
    status_id: Int!
    status_name: name!
  }

  # aggregated selection of "status"
  type status_aggregate {
    aggregate: status_aggregate_fields
    nodes: [status!]!
  }

  # aggregate fields of "status"
  type status_aggregate_fields {
    avg: status_avg_fields
    count(columns: [status_select_column!], distinct: Boolean): Int
    max: status_max_fields
    min: status_min_fields
    stddev: status_stddev_fields
    stddev_pop: status_stddev_pop_fields
    stddev_samp: status_stddev_samp_fields
    sum: status_sum_fields
    var_pop: status_var_pop_fields
    var_samp: status_var_samp_fields
    variance: status_variance_fields
  }

  # order by aggregate values of table "status"
  input status_aggregate_order_by {
    avg: status_avg_order_by
    count: order_by
    max: status_max_order_by
    min: status_min_order_by
    stddev: status_stddev_order_by
    stddev_pop: status_stddev_pop_order_by
    stddev_samp: status_stddev_samp_order_by
    sum: status_sum_order_by
    var_pop: status_var_pop_order_by
    var_samp: status_var_samp_order_by
    variance: status_variance_order_by
  }

  # input type for inserting array relation for remote table "status"
  input status_arr_rel_insert_input {
    data: [status_insert_input!]!
    on_conflict: status_on_conflict
  }

  # aggregate avg on columns
  type status_avg_fields {
    status_id: Float
  }

  # order by avg() on columns of table "status"
  input status_avg_order_by {
    status_id: order_by
  }

  # Boolean expression to filter rows from the table "status". All fields are combined with a logical 'AND'.
  input status_bool_exp {
    _and: [status_bool_exp]
    _not: status_bool_exp
    _or: [status_bool_exp]
    events: event_bool_exp
    status_id: Int_comparison_exp
    status_name: name_comparison_exp
  }

  # unique or primary key constraints on table "status"
  enum status_constraint {
    # unique or primary key constraint
    status_pkey

    # unique or primary key constraint
    status_status_name_key
  }

  # input type for incrementing integer column in table "status"
  input status_inc_input {
    status_id: Int
  }

  # input type for inserting data into table "status"
  input status_insert_input {
    events: event_arr_rel_insert_input
    status_id: Int
    status_name: name
  }

  # aggregate max on columns
  type status_max_fields {
    status_id: Int
  }

  # order by max() on columns of table "status"
  input status_max_order_by {
    status_id: order_by
  }

  # aggregate min on columns
  type status_min_fields {
    status_id: Int
  }

  # order by min() on columns of table "status"
  input status_min_order_by {
    status_id: order_by
  }

  # response of any mutation on the table "status"
  type status_mutation_response {
    # number of affected rows by the mutation
    affected_rows: Int!

    # data of the affected rows by the mutation
    returning: [status!]!
  }

  # input type for inserting object relation for remote table "status"
  input status_obj_rel_insert_input {
    data: status_insert_input!
    on_conflict: status_on_conflict
  }

  # on conflict condition type for table "status"
  input status_on_conflict {
    constraint: status_constraint!
    update_columns: [status_update_column!]!
    where: status_bool_exp
  }

  # ordering options when selecting data from "status"
  input status_order_by {
    events_aggregate: event_aggregate_order_by
    status_id: order_by
    status_name: order_by
  }

  # primary key columns input for table: "status"
  input status_pk_columns_input {
    status_id: Int!
  }

  # select columns of table "status"
  enum status_select_column {
    # column name
    status_id

    # column name
    status_name
  }

  # input type for updating data in table "status"
  input status_set_input {
    status_id: Int
    status_name: name
  }

  # aggregate stddev on columns
  type status_stddev_fields {
    status_id: Float
  }

  # order by stddev() on columns of table "status"
  input status_stddev_order_by {
    status_id: order_by
  }

  # aggregate stddev_pop on columns
  type status_stddev_pop_fields {
    status_id: Float
  }

  # order by stddev_pop() on columns of table "status"
  input status_stddev_pop_order_by {
    status_id: order_by
  }

  # aggregate stddev_samp on columns
  type status_stddev_samp_fields {
    status_id: Float
  }

  # order by stddev_samp() on columns of table "status"
  input status_stddev_samp_order_by {
    status_id: order_by
  }

  # aggregate sum on columns
  type status_sum_fields {
    status_id: Int
  }

  # order by sum() on columns of table "status"
  input status_sum_order_by {
    status_id: order_by
  }

  # update columns of table "status"
  enum status_update_column {
    # column name
    status_id

    # column name
    status_name
  }

  # aggregate var_pop on columns
  type status_var_pop_fields {
    status_id: Float
  }

  # order by var_pop() on columns of table "status"
  input status_var_pop_order_by {
    status_id: order_by
  }

  # aggregate var_samp on columns
  type status_var_samp_fields {
    status_id: Float
  }

  # order by var_samp() on columns of table "status"
  input status_var_samp_order_by {
    status_id: order_by
  }

  # aggregate variance on columns
  type status_variance_fields {
    status_id: Float
  }

  # order by variance() on columns of table "status"
  input status_variance_order_by {
    status_id: order_by
  }

  # expression to compare columns of type String. All fields are combined with logical 'AND'.
  input String_comparison_exp {
    _eq: String
    _gt: String
    _gte: String
    _ilike: String
    _in: [String!]
    _is_null: Boolean
    _like: String
    _lt: String
    _lte: String
    _neq: String
    _nilike: String
    _nin: [String!]
    _nlike: String
    _nsimilar: String
    _similar: String
  }

  # subscription root
  type subscription_root {
    # fetch data from the table: "event"
    event(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): [event!]!

    # fetch aggregated fields from the table: "event"
    event_aggregate(
      # distinct select on columns
      distinct_on: [event_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_order_by!]

      # filter the rows returned
      where: event_bool_exp
    ): event_aggregate!

    # fetch data from the table: "event" using primary key columns
    event_by_pk(event_id: Int!): event

    # fetch data from the table: "event_tag"
    event_tag(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): [event_tag!]!

    # fetch aggregated fields from the table: "event_tag"
    event_tag_aggregate(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): event_tag_aggregate!

    # fetch data from the table: "event_tag" using primary key columns
    event_tag_by_pk(event_tag_id: Int!): event_tag

    # fetch data from the table: "status"
    status(
      # distinct select on columns
      distinct_on: [status_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [status_order_by!]

      # filter the rows returned
      where: status_bool_exp
    ): [status!]!

    # fetch aggregated fields from the table: "status"
    status_aggregate(
      # distinct select on columns
      distinct_on: [status_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [status_order_by!]

      # filter the rows returned
      where: status_bool_exp
    ): status_aggregate!

    # fetch data from the table: "status" using primary key columns
    status_by_pk(status_id: Int!): status

    # fetch data from the table: "tag"
    tag(
      # distinct select on columns
      distinct_on: [tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [tag_order_by!]

      # filter the rows returned
      where: tag_bool_exp
    ): [tag!]!

    # fetch aggregated fields from the table: "tag"
    tag_aggregate(
      # distinct select on columns
      distinct_on: [tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [tag_order_by!]

      # filter the rows returned
      where: tag_bool_exp
    ): tag_aggregate!

    # fetch data from the table: "tag" using primary key columns
    tag_by_pk(tag_id: Int!): tag
  }

  # columns and relationships of "tag"
  type tag {
    # An array relationship
    event_tags(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): [event_tag!]!

    # An aggregated array relationship
    event_tags_aggregate(
      # distinct select on columns
      distinct_on: [event_tag_select_column!]

      # limit the number of rows returned
      limit: Int

      # skip the first n rows. Use only with order_by
      offset: Int

      # sort the rows by one or more columns
      order_by: [event_tag_order_by!]

      # filter the rows returned
      where: event_tag_bool_exp
    ): event_tag_aggregate!
    tag_id: Int!
    tag_name: name!
  }

  # aggregated selection of "tag"
  type tag_aggregate {
    aggregate: tag_aggregate_fields
    nodes: [tag!]!
  }

  # aggregate fields of "tag"
  type tag_aggregate_fields {
    avg: tag_avg_fields
    count(columns: [tag_select_column!], distinct: Boolean): Int
    max: tag_max_fields
    min: tag_min_fields
    stddev: tag_stddev_fields
    stddev_pop: tag_stddev_pop_fields
    stddev_samp: tag_stddev_samp_fields
    sum: tag_sum_fields
    var_pop: tag_var_pop_fields
    var_samp: tag_var_samp_fields
    variance: tag_variance_fields
  }

  # order by aggregate values of table "tag"
  input tag_aggregate_order_by {
    avg: tag_avg_order_by
    count: order_by
    max: tag_max_order_by
    min: tag_min_order_by
    stddev: tag_stddev_order_by
    stddev_pop: tag_stddev_pop_order_by
    stddev_samp: tag_stddev_samp_order_by
    sum: tag_sum_order_by
    var_pop: tag_var_pop_order_by
    var_samp: tag_var_samp_order_by
    variance: tag_variance_order_by
  }

  # input type for inserting array relation for remote table "tag"
  input tag_arr_rel_insert_input {
    data: [tag_insert_input!]!
    on_conflict: tag_on_conflict
  }

  # aggregate avg on columns
  type tag_avg_fields {
    tag_id: Float
  }

  # order by avg() on columns of table "tag"
  input tag_avg_order_by {
    tag_id: order_by
  }

  # Boolean expression to filter rows from the table "tag". All fields are combined with a logical 'AND'.
  input tag_bool_exp {
    _and: [tag_bool_exp]
    _not: tag_bool_exp
    _or: [tag_bool_exp]
    event_tags: event_tag_bool_exp
    tag_id: Int_comparison_exp
    tag_name: name_comparison_exp
  }

  # unique or primary key constraints on table "tag"
  enum tag_constraint {
    # unique or primary key constraint
    tag_pkey

    # unique or primary key constraint
    tag_tag_name_key
  }

  # input type for incrementing integer column in table "tag"
  input tag_inc_input {
    tag_id: Int
  }

  # input type for inserting data into table "tag"
  input tag_insert_input {
    event_tags: event_tag_arr_rel_insert_input
    tag_id: Int
    tag_name: name
  }

  # aggregate max on columns
  type tag_max_fields {
    tag_id: Int
  }

  # order by max() on columns of table "tag"
  input tag_max_order_by {
    tag_id: order_by
  }

  # aggregate min on columns
  type tag_min_fields {
    tag_id: Int
  }

  # order by min() on columns of table "tag"
  input tag_min_order_by {
    tag_id: order_by
  }

  # response of any mutation on the table "tag"
  type tag_mutation_response {
    # number of affected rows by the mutation
    affected_rows: Int!

    # data of the affected rows by the mutation
    returning: [tag!]!
  }

  # input type for inserting object relation for remote table "tag"
  input tag_obj_rel_insert_input {
    data: tag_insert_input!
    on_conflict: tag_on_conflict
  }

  # on conflict condition type for table "tag"
  input tag_on_conflict {
    constraint: tag_constraint!
    update_columns: [tag_update_column!]!
    where: tag_bool_exp
  }

  # ordering options when selecting data from "tag"
  input tag_order_by {
    event_tags_aggregate: event_tag_aggregate_order_by
    tag_id: order_by
    tag_name: order_by
  }

  # primary key columns input for table: "tag"
  input tag_pk_columns_input {
    tag_id: Int!
  }

  # select columns of table "tag"
  enum tag_select_column {
    # column name
    tag_id

    # column name
    tag_name
  }

  # input type for updating data in table "tag"
  input tag_set_input {
    tag_id: Int
    tag_name: name
  }

  # aggregate stddev on columns
  type tag_stddev_fields {
    tag_id: Float
  }

  # order by stddev() on columns of table "tag"
  input tag_stddev_order_by {
    tag_id: order_by
  }

  # aggregate stddev_pop on columns
  type tag_stddev_pop_fields {
    tag_id: Float
  }

  # order by stddev_pop() on columns of table "tag"
  input tag_stddev_pop_order_by {
    tag_id: order_by
  }

  # aggregate stddev_samp on columns
  type tag_stddev_samp_fields {
    tag_id: Float
  }

  # order by stddev_samp() on columns of table "tag"
  input tag_stddev_samp_order_by {
    tag_id: order_by
  }

  # aggregate sum on columns
  type tag_sum_fields {
    tag_id: Int
  }

  # order by sum() on columns of table "tag"
  input tag_sum_order_by {
    tag_id: order_by
  }

  # update columns of table "tag"
  enum tag_update_column {
    # column name
    tag_id

    # column name
    tag_name
  }

  # aggregate var_pop on columns
  type tag_var_pop_fields {
    tag_id: Float
  }

  # order by var_pop() on columns of table "tag"
  input tag_var_pop_order_by {
    tag_id: order_by
  }

  # aggregate var_samp on columns
  type tag_var_samp_fields {
    tag_id: Float
  }

  # order by var_samp() on columns of table "tag"
  input tag_var_samp_order_by {
    tag_id: order_by
  }

  # aggregate variance on columns
  type tag_variance_fields {
    tag_id: Float
  }

  # order by variance() on columns of table "tag"
  input tag_variance_order_by {
    tag_id: order_by
  }

  scalar timestamptz

  # expression to compare columns of type timestamptz. All fields are combined with logical 'AND'.
  input timestamptz_comparison_exp {
    _eq: timestamptz
    _gt: timestamptz
    _gte: timestamptz
    _in: [timestamptz!]
    _is_null: Boolean
    _lt: timestamptz
    _lte: timestamptz
    _neq: timestamptz
    _nin: [timestamptz!]
  }
`;

export default typeDefs;