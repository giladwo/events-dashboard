import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { InMemoryCache, ApolloProvider, createHttpLink, split } from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from '@apollo/client/utilities';
import ApolloClient from 'apollo-client';

import resolvers from './apollo/resolvers'
import typeDefs from './apollo/schema';

const cache = new InMemoryCache();

const wsLink = new WebSocketLink({
  uri: "ws://localhost:8080/v1/graphql",
  options: {
    reconnect: true,
  }
});

const httpLink = createHttpLink({
  uri: "http://localhost:8080/v1/graphql",
});

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === "OperationDefinition" && operation === "subscription";
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  cache,
  link,
  resolvers,
  typeDefs,
  connectToDevTools: true
})

// TODO: replace with writeQuery (changed in Apollo v3.0)
// cache.writeData({
//   data: {
//     welcomeMessage: "Welcome to my React & Apollo template ❤️"
//   }
// })

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);
